package org.systemia.book.infrastructure.handlers;

import jakarta.mail.MessagingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.systemia.book.exceptions.OperationNotPermittedException;

import java.util.HashSet;
import java.util.Set;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestControllerAdvice
public class GenericExceptionHandler {

	@ExceptionHandler(LockedException.class)
	public ResponseEntity<ExceptionResponse> handleException(LockedException ex) {
		log.error(ex.getMessage());
		return ResponseEntity.status(UNAUTHORIZED)
			.body(
				ExceptionResponse.builder()
					.businessErrorCode(BusinessErrorCodes.ACCOUNT_LOCKED.getCode())
					.businessExceptionDescription(BusinessErrorCodes.ACCOUNT_LOCKED.getDescription())
					.error(ex.getMessage())
					.build()
			);
	}

	@ExceptionHandler(DisabledException.class)
	public ResponseEntity<ExceptionResponse> handleException(DisabledException ex) {
		log.error(ex.getMessage());
		return ResponseEntity.status(UNAUTHORIZED)
			.body(
				ExceptionResponse.builder()
					.businessErrorCode(BusinessErrorCodes.ACCOUNT_DISABLED.getCode())
					.businessExceptionDescription(BusinessErrorCodes.ACCOUNT_DISABLED.getDescription())
					.error(ex.getMessage())
					.build()
			);
	}

	@ExceptionHandler(BadCredentialsException.class)
	public ResponseEntity<ExceptionResponse> handleException(BadCredentialsException ex) {
		log.error(ex.getMessage());
		return ResponseEntity.status(UNAUTHORIZED)
			.body(
				ExceptionResponse.builder()
					.businessErrorCode(BusinessErrorCodes.BAD_CREDENTIALS.getCode())
					.businessExceptionDescription(BusinessErrorCodes.BAD_CREDENTIALS.getDescription())
					.error(BusinessErrorCodes.BAD_CREDENTIALS.getDescription())
					.build()
			);
	}

	@ExceptionHandler(MessagingException.class)
	public ResponseEntity<ExceptionResponse> handleException(MessagingException ex) {
		log.error(ex.getMessage());
		return ResponseEntity.status(INTERNAL_SERVER_ERROR)
			.body(
				ExceptionResponse.builder()
					.error(ex.getMessage())
					.build()
			);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ExceptionResponse> handleException(MethodArgumentNotValidException ex) {
		Set<String> errors = new HashSet<>();

		ex.getBindingResult().getAllErrors()
			.forEach(error -> {
				var errorMessage = error.getDefaultMessage();
				errors.add(errorMessage);
			});

		log.error(ex.getMessage());
		return ResponseEntity.status(BAD_REQUEST)
			.body(
				ExceptionResponse.builder()
					.validationErrors(errors)
					.build()
			);
	}

	@ExceptionHandler(OperationNotPermittedException.class)
	public ResponseEntity<ExceptionResponse> handleException(OperationNotPermittedException ex) {
		log.error(ex.getMessage());
		return ResponseEntity.status(BAD_REQUEST)
			.body(
				ExceptionResponse.builder()
					.error(ex.getMessage())
					.build()
			);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ExceptionResponse> handleException(Exception ex) {
		log.error(ex.getMessage());
		return ResponseEntity.status(INTERNAL_SERVER_ERROR)
			.body(
				ExceptionResponse.builder()
					.businessExceptionDescription("Internal error, contact the admin")
					.error(ex.getMessage())
					.build()
			);
	}
}
