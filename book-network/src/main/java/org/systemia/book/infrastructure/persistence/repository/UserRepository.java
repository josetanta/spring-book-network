package org.systemia.book.infrastructure.persistence.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import org.systemia.book.infrastructure.persistence.entities.User;

import java.util.Optional;

/**
 * Repository interface for {@link User} entity operations, extending {@link JpaRepository}.
 * This repository provides basic CRUD operations and custom query methods.
 *
 * <p>Uses Spring Data JPA to perform database interactions and includes an entity graph to eagerly load
 * the associated "roles" collection when finding a user by email.</p>
 *
 * @see JpaRepository
 * @see User
 */
public interface UserRepository extends JpaRepository<User, Long> {

	/**
	 * Retrieves a {@link User} entity by its email. The "roles" attribute is eagerly loaded using
	 * an {@link EntityGraph} to improve performance by avoiding additional queries for roles.
	 *
	 * @param email the email of the user to be retrieved.
	 * @return an {@link Optional} containing the user if found, or {@link Optional#empty()} otherwise.
	 */
	@EntityGraph(attributePaths = "roles")
	Optional<User> findByEmail(@NonNull String email);
}

