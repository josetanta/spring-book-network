package org.systemia.book.infrastructure.persistence.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name = "tokens")
public class Token {

	@Id
	@GeneratedValue
	private Integer id;
	private String tokenHash;
	private LocalDateTime createdAt;
	private LocalDateTime updatedAt;
	private LocalDateTime validatedAt;
	private LocalDateTime expiresAt;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;
}
