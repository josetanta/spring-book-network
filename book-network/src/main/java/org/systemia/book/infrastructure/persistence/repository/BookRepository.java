package org.systemia.book.infrastructure.persistence.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;
import org.systemia.book.infrastructure.persistence.entities.Book;

import java.util.stream.Stream;

public interface BookRepository extends JpaRepository<Book, Long>, JpaSpecificationExecutor<Book> {

	@Query("""
		FROM Book b
			WHERE b.archived = false AND
			b.shareable = true AND
			b.owner.id != :ownerId
		""")
	Page<Book> findAllDisplayableBooks(Pageable pageable, Long ownerId);

	@Override
	@EntityGraph(value = "Book.authorAndFeedback")
	Page<Book> findAll(@NonNull Specification<Book> spec, @NonNull Pageable pageable);

	Stream<Book> streamAllBy();
}
