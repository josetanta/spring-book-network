package org.systemia.book.infrastructure.persistence.entities;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.systemia.book.infrastructure.persistence.entities.common.BaseEntity;

import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "books")
@NamedEntityGraphs({
	@NamedEntityGraph(
		name = "Book.authorAndFeedback",
		attributeNodes = {
			@NamedAttributeNode("owner"),
			@NamedAttributeNode("feedbacks")
		}
	)
})
public class Book extends BaseEntity {

	/**
	 * Relationship with User
	 */
	public static final String R_OWNER = "owner";

	private String title;

	private String authorName;

	private String isbn;

	private String synopsis;

	private String bookCover;

	@Lob
	private byte[] cover;

	private boolean archived;

	private boolean shareable;

	@ManyToOne
	@JoinColumn(name = "owner_id")
	private User owner;

	@ToString.Exclude
	@OneToMany(mappedBy = "book")
	private List<Feedback> feedbacks;

	@ToString.Exclude
	@OneToMany(mappedBy = "book")
	private List<BookTransactionHistory> histories;

	@Transient
	public double getRate() {
		if (Objects.isNull(feedbacks) || feedbacks.isEmpty()) {
			return 0.0;
		}

		var rate = this.feedbacks.stream()
			.mapToDouble(Feedback::getNote)
			.average()
			.orElse(0.0);

		// 3.23 --> 3.0 || 3.65 --> 4.0
		return Math.round(rate * 10.0) / 10.0;
	}
}
