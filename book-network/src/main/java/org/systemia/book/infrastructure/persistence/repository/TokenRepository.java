package org.systemia.book.infrastructure.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.systemia.book.infrastructure.persistence.entities.Token;

import java.util.Optional;

public interface TokenRepository extends JpaRepository<Token, Integer> {
	Optional<Token> findByTokenHash(String tokenHash);
}
