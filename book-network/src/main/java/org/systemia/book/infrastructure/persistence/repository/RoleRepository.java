package org.systemia.book.infrastructure.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.systemia.book.infrastructure.persistence.entities.Role;

import java.util.Optional;


public interface RoleRepository extends JpaRepository<Role, Integer> {
	Optional<Role> findByName(String name);
}
