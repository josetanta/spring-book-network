package org.systemia.book.infrastructure.config.auditing;

import org.springframework.data.domain.AuditorAware;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.systemia.book.infrastructure.persistence.entities.User;

import java.util.Objects;
import java.util.Optional;

public class ApplicationAuditAware implements AuditorAware<Long> {

	@Override
	@NonNull
	public Optional<Long> getCurrentAuditor() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (Objects.isNull(authentication) || !authentication.isAuthenticated() || authentication instanceof AnonymousAuthenticationToken) {
			return Optional.empty();
		}

		User userPrincipal = (User) authentication.getPrincipal();

		return Optional.ofNullable(userPrincipal.getId());
	}
}
