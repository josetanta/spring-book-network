package org.systemia.book.infrastructure.config.openapi;


import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.servers.Server;
import org.springframework.context.annotation.Configuration;

@Configuration
@OpenAPIDefinition(
	info = @Info(
		contact = @Contact(
			name = "Jose Tanta",
			email = "josetanta.es@gmail.com",
			url = "https://josetanta.es"
		),
		description = "OpenAPI documentation for Spring security",
		title = "OpenAPI specification - josetanta",
		version = "1.0",
		license = @License(
			name = "Licence name",
			url = "https://some-url.com"
		),
		termsOfService = "Terms of service"
	),
	servers = {
		@Server(
			description = "Local ENV",
			url = "http://localhost:8088/api/v1"
		),
		@Server(
			description = "Production ENV",
			url = "https://book-social.net/api/v1"
		)
	},
	security = {
		@SecurityRequirement(
			name = "bearerAuth"
		)
	}
)
@SecurityScheme(
	name = "bearerAuth",
	description = "JWT auth description",
	scheme = "bearer",
	type = SecuritySchemeType.HTTP,
	bearerFormat = "JWT",
	in = SecuritySchemeIn.HEADER
)
public class OpenApiConfig {
}
