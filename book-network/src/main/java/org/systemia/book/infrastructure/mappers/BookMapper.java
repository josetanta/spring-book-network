package org.systemia.book.infrastructure.mappers;

import org.mapstruct.Mapper;
import org.systemia.book.application.api.dtos.BookDTO;
import org.systemia.book.application.api.dtos.response.BookResponseDTO;
import org.systemia.book.application.api.dtos.response.BorrowedBookResponseDTO;
import org.systemia.book.infrastructure.persistence.entities.Book;
import org.systemia.book.infrastructure.persistence.entities.BookTransactionHistory;


@Mapper(componentModel = "spring")
public interface BookMapper extends GenericMapper<BookDTO, Book> {

	default BookResponseDTO toBookResponse(Book book) {
		return BookResponseDTO.builder()
			.id(book.getId())
			.title(book.getTitle())
			.authorName(book.getAuthorName())
			.isbn(book.getIsbn())
			.synopsis(book.getSynopsis())
			.rate(book.getRate())
			.archived(book.isArchived())
			.shareable(book.isShareable())
			.owner(book.getOwner().fullName())
			//	.cover(FileUtils.readFileFromLocation(book.getBookCover()))
			.imageBytes(book.getCover())
			.build();
	}

	default BorrowedBookResponseDTO toBorrowedBookResponse(BookTransactionHistory history) {
		return BorrowedBookResponseDTO.builder()
			.id(history.getBook().getId())
			.title(history.getBook().getTitle())
			.authorName(history.getBook().getAuthorName())
			.isbn(history.getBook().getIsbn())
			.rate(history.getBook().getRate())
			.returnApproved(history.isReturnApproved())
			.returned(history.isReturned())
			.build();
	}
}
