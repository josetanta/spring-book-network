package org.systemia.book.infrastructure.persistence.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.systemia.book.infrastructure.persistence.entities.Feedback;

public interface FeedbackRepository extends JpaRepository<Feedback, Long> {

	@Query("""
		SELECT f
		FROM Feedback f
		WHERE f.book.id = :bookId
		""")
	Page<Feedback> findAllByBook(Pageable pageable, Long bookId);
}
