package org.systemia.book.infrastructure.config.beans;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.AuditorAware;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.systemia.book.infrastructure.config.auditing.ApplicationAuditAware;

import java.util.Arrays;
import java.util.Collections;

@Order(1)
@Configuration
@RequiredArgsConstructor
public class BeansConfig {

	private final UserDetailsService userDetailsService;

	@Bean
	AuthenticationProvider authenticationProvider() {
		var daoAuthProvider = new DaoAuthenticationProvider();
		daoAuthProvider.setUserDetailsService(userDetailsService);
		daoAuthProvider.setPasswordEncoder(passwordEncoder());
		return daoAuthProvider;
	}

	@Bean
	AuthenticationManager authenticationManager(AuthenticationConfiguration conf) throws Exception {
		return conf.getAuthenticationManager();
	}

	@Bean
	PasswordEncoder passwordEncoder() {
		return Argon2PasswordEncoder.defaultsForSpringSecurity_v5_8();
	}

	@Bean
	AuditorAware<Long> auditorAware() {
		return new ApplicationAuditAware();
	}

	@Bean
	public CorsFilter corsFilter() {
		final var source = new UrlBasedCorsConfigurationSource();
		final var config = new CorsConfiguration();

		config.setAllowCredentials(true);
		config.setAllowedOrigins(Collections.singletonList("*"));
		config.setAllowedHeaders(Arrays.asList(
			HttpHeaders.ORIGIN,
			HttpHeaders.CONTENT_TYPE,
			HttpHeaders.ACCEPT,
			HttpHeaders.AUTHORIZATION
		));
		config.setAllowedMethods(Arrays.asList(
			"GET", "POST", "DELETE", "PUT", "PATCH"
		));
		source.registerCorsConfiguration("/**", config);
		return new CorsFilter(source);
	}
}
