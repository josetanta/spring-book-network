package org.systemia.book.infrastructure.mappers;

import org.mapstruct.Mapper;
import org.systemia.book.application.api.dtos.FeedbackDTO;
import org.systemia.book.application.api.dtos.response.FeedbackResponseDTO;
import org.systemia.book.infrastructure.persistence.entities.Book;
import org.systemia.book.infrastructure.persistence.entities.Feedback;

import java.util.Objects;


@Mapper(componentModel = "spring")
public interface FeedbackMapper extends GenericMapper<FeedbackDTO, Feedback> {

	@Override
	default Feedback toEntity(FeedbackDTO dto) {
		return Feedback.builder()
			.note(dto.note())
			.comment(dto.comment())
			.book(Book.builder()
				.id(dto.bookId())
				.build()
			)
			.build();
	}

	default FeedbackResponseDTO toResponseDTO(Feedback feedback, Long userId) {
		return FeedbackResponseDTO.builder()
			.note(feedback.getNote())
			.comment(feedback.getComment())
			.ownFeedback(Objects.equals(feedback.getCreatedBy(), userId))
			.build();
	}

}
