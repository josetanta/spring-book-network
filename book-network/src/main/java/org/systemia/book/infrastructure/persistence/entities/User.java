package org.systemia.book.infrastructure.persistence.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

//@NamedEntityGraph(
//	name = "user-roles",
//	attributeNodes = {
//		@NamedAttributeNode("roles")
//	}
//)
@Entity
@Table(name = "users")
@EntityListeners(AuditingEntityListener.class)
public class User implements UserDetails, Principal {

	@Id
	@GeneratedValue
	private Long id;

	private String firstname;

	private String lastname;

	private LocalDate dateOfBirth;

	@Column(unique = true)
	private String email;

	@ToString.Exclude
	private String passwordHash;

	private boolean accountLocked;

	private boolean enabled;

	// private list route

	@CreatedDate
	@Column(nullable = false, updatable = false)
	private LocalDateTime createdAt;

	@LastModifiedDate
	@Column(insertable = false)
	private LocalDateTime updatedAt;

	// Relationship
	@ToString.Exclude
	@ManyToMany
	//	@JsonManagedReference
	@OrderBy("name")
	private List<Role> roles = new ArrayList<>();

	@ToString.Exclude
	@OneToMany(mappedBy = "owner")
	private List<Book> books;

	@ToString.Exclude
	@OneToMany(mappedBy = "user")
	private List<BookTransactionHistory> histories;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName())).toList();
	}

	@Override
	public String getPassword() {
		return passwordHash;
	}

	@Override
	public String getUsername() {
		return email;
	}

	@Override
	public String getName() {
		return email;
	}

	@Override
	public boolean isAccountNonLocked() {
		return !accountLocked;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	public String fullName() {
		return firstname + " " + lastname;
	}
}


