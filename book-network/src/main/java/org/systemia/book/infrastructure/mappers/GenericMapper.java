package org.systemia.book.infrastructure.mappers;

import org.mapstruct.Context;
import org.springframework.data.domain.Page;
import org.systemia.book.utils.CycleAvoidingMappingContext;

import java.util.List;

/**
 * GenericMapper is a generic interface for mapping between DTO (Data Transfer Object)
 * and Entity objects. This interface provides methods to convert entities to DTOs
 * and DTOs to entities, including handling collections and pagination.
 *
 * @param <D> The type parameter representing the DTO.
 * @param <E> The type parameter representing the Entity.
 */
public interface GenericMapper<D, E> {

	/**
	 * Converts an entity to a DTO.
	 *
	 * @param entity The entity to convert to a DTO.
	 * @return The converted DTO.
	 */
	D toDTO(E entity);

	/**
	 * Converts an entity to a DTO with a context to avoid cycles in bidirectional relationships.
	 *
	 * @param entity  The entity to convert to a DTO.
	 * @param context The context for cycle avoidance in mapping.
	 * @return The converted DTO.
	 */
	D toDTO(E entity, @Context CycleAvoidingMappingContext context);

	/**
	 * Converts a DTO to an entity.
	 *
	 * @param dto The DTO to convert to an entity.
	 * @return The converted entity.
	 */
	E toEntity(D dto);

	/**
	 * Converts a DTO to an entity with a context to avoid cycles in bidirectional relationships.
	 *
	 * @param dto     The DTO to convert to an entity.
	 * @param context The context for cycle avoidance in mapping.
	 * @return The converted entity.
	 */
	E toEntity(D dto, @Context CycleAvoidingMappingContext context);

	/**
	 * Converts a list of entities to a list of DTOs.
	 *
	 * @param entity The list of entities to convert.
	 * @return The list of converted DTOs.
	 */
	List<D> toDTO(List<E> entity);

	/**
	 * Converts a list of DTOs to a list of entities.
	 *
	 * @param dto The list of DTOs to convert.
	 * @return The list of converted entities.
	 */
	List<E> toEntity(List<D> dto);

	/**
	 * Converts a page of entities to a page of DTOs.
	 *
	 * @param entities The page of entities to convert.
	 * @return The page of converted DTOs.
	 */
	default Page<D> toDTO(Page<E> entities) {
		return entities.map(this::toDTO);
	}
}
