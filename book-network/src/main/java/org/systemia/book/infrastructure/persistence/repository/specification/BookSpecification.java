package org.systemia.book.infrastructure.persistence.repository.specification;

import jakarta.persistence.criteria.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;
import org.systemia.book.infrastructure.persistence.entities.Book;
import org.systemia.book.infrastructure.persistence.entities.User;

import java.util.ArrayList;

@Slf4j
public final class BookSpecification {

	private BookSpecification() {
	}

	public static Specification<Book> withOwnerId(Long ownerId) {
		log.info("Filtering withOwnerId: {}", ownerId);
		return (entity, query, cb) -> cb.equal(entity.get(Book.R_OWNER).get("id"), ownerId);
	}

	public static Specification<Book> withOwnerName(Long ownerId, String ownerName) {
		return (entity, query, cb) -> {
			var predicates = new ArrayList<Predicate>();

			filteringDefault(ownerId, entity, cb, predicates);
			filteringByOwnerNameAndLastname(ownerName, entity, cb, predicates);

			return cb.and(predicates.toArray(new Predicate[0]));
		};
	}

	private static void filteringByOwnerNameAndLastname(String ownerName, Root<Book> entity, CriteriaBuilder cb, ArrayList<Predicate> predicates) {
		var predicatesAuthorName = new ArrayList<Predicate>();
		Join<User, Book> owner = entity.join("owner", JoinType.LEFT);
		predicatesAuthorName.add(cb.like(owner.get("firstname"), "%" + ownerName + "%"));
		predicatesAuthorName.add(cb.like(owner.get("lastname"), "%" + ownerName + "%"));

		predicates.add(cb.or(predicatesAuthorName.toArray(new Predicate[0])));
	}

	private static void filteringDefault(Long userId, Root<Book> entity, CriteriaBuilder cb, ArrayList<Predicate> predicates) {
		var predicatesDefault = new ArrayList<Predicate>();
		predicatesDefault.add(
			cb.equal(entity.get("archived").as(Boolean.class), false)
		);

		predicatesDefault.add(
			cb.equal(entity.get("shareable").as(Boolean.class), true)
		);

		Join<User, Book> owner = entity.join("owner", JoinType.LEFT);
		predicatesDefault.add(
			cb.equal(owner.get("id"), userId)
		);

		predicates.add(cb.and(predicatesDefault.toArray(new Predicate[0])));
	}
}
