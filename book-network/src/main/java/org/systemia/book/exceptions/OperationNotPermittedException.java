package org.systemia.book.exceptions;

public class OperationNotPermittedException extends RuntimeException {
	public OperationNotPermittedException(String s) {
		super(s);
	}
}
