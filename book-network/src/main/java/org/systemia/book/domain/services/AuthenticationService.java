package org.systemia.book.domain.services;

import jakarta.mail.MessagingException;
import org.systemia.book.application.api.dtos.AuthenticationDTO;
import org.systemia.book.application.api.dtos.UserRegisterDTO;
import org.systemia.book.application.api.dtos.response.TokenResponseDTO;

/**
 * Service for authentication user
 */
public interface AuthenticationService {

	/**
	 * Method for saving user
	 *
	 * @param dto data transfer object
	 */
	Long createUser(final UserRegisterDTO dto) throws MessagingException;

	/**
	 * Method for authentication
	 *
	 * @param dto
	 * @return a dto with token
	 */
	TokenResponseDTO authenticate(final AuthenticationDTO dto);

	/**
	 * Method for activate account, enabled account
	 *
	 * @param token
	 */
	void activateAccount(final String token) throws MessagingException;
}
