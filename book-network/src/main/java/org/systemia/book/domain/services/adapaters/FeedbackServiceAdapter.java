package org.systemia.book.domain.services.adapaters;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.systemia.book.application.api.dtos.FeedbackDTO;
import org.systemia.book.application.api.dtos.response.FeedbackResponseDTO;
import org.systemia.book.domain.services.FeedbackService;
import org.systemia.book.exceptions.OperationNotPermittedException;
import org.systemia.book.infrastructure.mappers.FeedbackMapper;
import org.systemia.book.infrastructure.persistence.entities.Book;
import org.systemia.book.infrastructure.persistence.entities.Feedback;
import org.systemia.book.infrastructure.persistence.entities.User;
import org.systemia.book.infrastructure.persistence.repository.BookRepository;
import org.systemia.book.infrastructure.persistence.repository.FeedbackRepository;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class FeedbackServiceAdapter implements FeedbackService {

	private final BookRepository bookRepository;
	private final FeedbackMapper feedbackMapper;
	private final FeedbackRepository feedbackRepository;


	@Override
	public Long save(FeedbackDTO dto, Authentication connectedUser) {
		Book book = bookRepository.findById(dto.bookId())
			.orElseThrow(() -> new EntityNotFoundException("No book found with the ID::" + dto.bookId()));

		if (book.isArchived() || !book.isShareable()) {
			throw new OperationNotPermittedException("You cannot give a feedback for an archived or not shareable book");
		}

		User user = (User) connectedUser.getPrincipal();
		if (!Objects.equals(book.getOwner().getId(), user.getId())) {
			throw new OperationNotPermittedException("You cannot give a feedback to your book");
		}

		Feedback feedback = feedbackMapper.toEntity(dto);
		return feedbackRepository.save(feedback).getId();
	}

	@Override
	public Page<FeedbackResponseDTO> findAllFeedbacksByBook(Long bookId, Pageable pageable, Authentication connectedUser) {
		User user = (User) connectedUser.getPrincipal();
		Page<Feedback> feedbackPage = feedbackRepository.findAllByBook(pageable, bookId);

		return feedbackPage.map(f -> feedbackMapper.toResponseDTO(f, user.getId()));
	}
}
