package org.systemia.book.domain.services;

import jakarta.mail.MessagingException;
import org.systemia.book.utils.EmailTemplateName;

/**
 * Email service for send mail
 *
 * @author josetanta
 */
public interface EmailService {

	/**
	 * Method for sending mail
	 *
	 * @param to
	 * @param username
	 * @param emailTemplateName
	 * @param confirmationUrl
	 * @param activationCode
	 * @param subject
	 * @throws MessagingException
	 */
	void sendEmail(String to, String username, EmailTemplateName emailTemplateName, String confirmationUrl, String activationCode, String subject) throws MessagingException;

}
