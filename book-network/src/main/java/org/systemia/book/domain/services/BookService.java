package org.systemia.book.domain.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;
import org.systemia.book.application.api.dtos.BookDTO;
import org.systemia.book.application.api.dtos.response.BookResponseDTO;
import org.systemia.book.application.api.dtos.response.BorrowedBookResponseDTO;

import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

/**
 * Service for entity {@link org.systemia.book.infrastructure.persistence.entities.Book}
 *
 * @author josetanta
 */
public interface BookService {

	/**
	 * Method for saving book
	 *
	 * @param dto for create entity
	 * @return a integer
	 */
	Long createBook(BookDTO dto, Authentication authentication);

	/**
	 * Method for finding a book by ID
	 *
	 * @param bookId of book
	 * @return DTO
	 */
	BookResponseDTO findByID(Long bookId);

	/**
	 * Method for pagination entities
	 *
	 * @param pageable      page request
	 * @param connectedUser user authenticated
	 * @return Page<DTO> BookResponseDTO
	 */
	Page<BookResponseDTO> findAllBooks(Pageable pageable, Authentication connectedUser);

	/**
	 * Method for searching and pagination
	 *
	 * @param pageable      page request
	 * @param connectedUser user authenticated
	 * @return Page<DTO> BookResponseDTO
	 */
	Page<BookResponseDTO> findAllBooksByOwner(Pageable pageable, Authentication connectedUser);

	/**
	 * Method for searching and pagination books borrowed
	 *
	 * @param pageable      page request
	 * @param connectedUser user authenticated
	 * @return Page<BorrowedBookResponseDTO>
	 */
	Page<BorrowedBookResponseDTO> findAllBorrowedBooks(Pageable pageable, Authentication connectedUser);

	/**
	 * Method for searching and pagination books returned
	 *
	 * @param pageable      page request
	 * @param connectedUser user authenticated
	 * @return Page<BorrowedBookResponseDTO>
	 */
	Page<BorrowedBookResponseDTO> findAllReturnedBooks(Pageable pageable, Authentication connectedUser);

	/**
	 * Method for updating shareable status of Book
	 *
	 * @param bookId        identifier of book
	 * @param connectedUser user authenticated
	 * @return long
	 */
	Long updateShareableStatus(Long bookId, Authentication connectedUser);

	/**
	 * Method for updating archived status of Book
	 *
	 * @param bookId        identifier of book
	 * @param connectedUser user authenticated
	 * @return long
	 */
	Long updateArchivedStatus(Long bookId, Authentication connectedUser);

	/**
	 * Method for borrowing a book
	 *
	 * @param bookId        identifier of book
	 * @param connectedUser user authenticated
	 * @return long
	 */
	Long borrowBook(Long bookId, Authentication connectedUser);

	/**
	 * Method for returning borrow a book
	 *
	 * @param bookId        identifier of book
	 * @param connectedUser user authenticated
	 * @return long
	 */
	Long returnBorrowedBook(Long bookId, Authentication connectedUser);

	List<BookResponseDTO> streamAll();

	/**
	 * Method for approving and return borrowed book
	 *
	 * @param bookId        identifier of book
	 * @param connectedUser user authenticated
	 * @return long
	 */
	Long approveReturnBorrowedBook(Long bookId, Authentication connectedUser);

	/**
	 * Method for uploading a picture with MultipartFile
	 *
	 * @param multipartFile  file send from client
	 * @param connectedIUser user authenticated
	 * @param bookId         identifier of book
	 */
	void uploadBookCoverPicture(MultipartFile multipartFile, Authentication connectedIUser, Long bookId, double resize) throws IOException;
}
