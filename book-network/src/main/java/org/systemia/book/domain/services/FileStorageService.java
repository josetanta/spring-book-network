package org.systemia.book.domain.services;

import jakarta.annotation.Nonnull;
import org.springframework.web.multipart.MultipartFile;
import org.systemia.book.infrastructure.persistence.entities.Book;

/**
 * Service for storage objects
 *
 * @author josetanta
 */
public interface FileStorageService {

	/**
	 * Method for saving a object (file) from book with user
	 *
	 * @param sourceFile file
	 * @param book       Book id
	 * @param userId     User Id
	 * @return name of object
	 */
	String saveFile(@Nonnull MultipartFile sourceFile, @Nonnull Book book, @Nonnull Long userId);
}
