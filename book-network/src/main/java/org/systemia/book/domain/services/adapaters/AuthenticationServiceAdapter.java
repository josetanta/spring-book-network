package org.systemia.book.domain.services.adapaters;

import jakarta.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.systemia.book.application.api.dtos.AuthenticationDTO;
import org.systemia.book.application.api.dtos.UserRegisterDTO;
import org.systemia.book.application.api.dtos.response.TokenResponseDTO;
import org.systemia.book.domain.services.AuthenticationService;
import org.systemia.book.domain.services.EmailService;
import org.systemia.book.infrastructure.config.security.JwtService;
import org.systemia.book.infrastructure.persistence.entities.Token;
import org.systemia.book.infrastructure.persistence.entities.User;
import org.systemia.book.infrastructure.persistence.repository.RoleRepository;
import org.systemia.book.infrastructure.persistence.repository.TokenRepository;
import org.systemia.book.infrastructure.persistence.repository.UserRepository;
import org.systemia.book.utils.EmailTemplateName;

import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthenticationServiceAdapter implements AuthenticationService {

	private final RoleRepository roleRepository;
	private final UserRepository userRepository;
	private final TokenRepository tokenRepository;
	private final PasswordEncoder passwordEncoder;
	private final EmailService emailService;
	private final AuthenticationProvider authenticationProvider;
	private final JwtService jwtService;

	@Value("${application.mailing.frontend.activation-url}")
	private String activationUrl;

	@Override
	public Long createUser(UserRegisterDTO dto) throws MessagingException {
		var userRole = roleRepository.findByName("USER")
			.orElseThrow(() -> new IllegalStateException("Role USER was not initialzed"));

		var user = User.builder()
			.firstname(dto.firstname())
			.lastname(dto.lastname())
			.passwordHash(passwordEncoder.encode(dto.password()))
			.email(dto.email())
			.accountLocked(false)
			.enabled(false)
			.roles(List.of(userRole))
			.build();

		User userCreated = userRepository.save(user);
		sendValidationEmail(user);
		log.info("User registered");
		return userCreated.getId();
	}

	@Override
	public TokenResponseDTO authenticate(AuthenticationDTO dto) {
		log.info("Authentication user {}", dto);
		Authentication auth = authenticationProvider.authenticate(
			new UsernamePasswordAuthenticationToken(
				dto.email(),
				dto.password()
			)
		);

		var claims = new HashMap<String, Object>();
		var user = ((User) auth.getPrincipal());
		claims.put("fullName", user.fullName());
		var jwtToken = jwtService.generateToken(claims, user);

		return new TokenResponseDTO(jwtToken);
	}

	@Override
	public void activateAccount(final String token) throws MessagingException {
		Token savedToken = tokenRepository.findByTokenHash(token).orElseThrow(() -> new IllegalStateException("Invalid token"));

		if (LocalDateTime.now().isAfter(savedToken.getExpiresAt())) {
			sendValidationEmail(savedToken.getUser());
			throw new IllegalStateException("Activation token has expired. A new token has been sent to the email address");
		}

		var user = userRepository.findById(savedToken.getUser().getId())
			.orElseThrow(() -> new UsernameNotFoundException("User not found"));

		user.setEnabled(true);
		userRepository.save(user);
		savedToken.setValidatedAt(LocalDateTime.now());
		tokenRepository.save(savedToken);
	}

	private void sendValidationEmail(User user) throws MessagingException {
		var newToken = generateAndSaveActivationToken(user);
		emailService.sendEmail(user.getEmail(), user.fullName(), EmailTemplateName.ACTIVATE_ACCOUNT, activationUrl, newToken, "Account activation");
	}

	private String generateAndSaveActivationToken(User user) {
		String generateToken = generateActivationCode(6);
		var token = Token.builder().tokenHash(generateToken).createdAt(LocalDateTime.now()).expiresAt(LocalDateTime.now().plusMinutes(18)).user(user).build();
		tokenRepository.save(token);
		return generateToken;
	}

	private String generateActivationCode(int length) {
		var numbers = "0123456789";
		var codeBuilder = new StringBuilder();
		var secureRandom = new SecureRandom();

		for (int i = 0; i < length; i++) {
			int randomIndex = secureRandom.nextInt(numbers.length());
			codeBuilder.append(numbers.charAt(randomIndex));
		}

		return codeBuilder.toString();
	}
}
