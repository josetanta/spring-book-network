package org.systemia.book.domain.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.systemia.book.application.api.dtos.FeedbackDTO;
import org.systemia.book.application.api.dtos.response.FeedbackResponseDTO;

/**
 * Service for the entity Feedback
 *
 * @author josetanta
 */
public interface FeedbackService {

	/**
	 * Method for save or persist a new feedback from book-id
	 *
	 * @param dto           Data from request
	 * @param connectedUser user authenticated
	 * @return long an id feedback was saved
	 */
	Long save(FeedbackDTO dto, Authentication connectedUser);


	/**
	 * Method for searching by book and paginated
	 *
	 * @param bookId        identifier of book
	 * @param pageable      pagination and sorting
	 * @param connectedUser user authenticated
	 * @return Page<FeedbackResponseDTO>
	 */
	Page<FeedbackResponseDTO> findAllFeedbacksByBook(Long bookId, Pageable pageable, Authentication connectedUser);
}
