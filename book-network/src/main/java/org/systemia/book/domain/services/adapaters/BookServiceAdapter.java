package org.systemia.book.domain.services.adapaters;


import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.systemia.book.application.api.dtos.BookDTO;
import org.systemia.book.application.api.dtos.response.BookResponseDTO;
import org.systemia.book.application.api.dtos.response.BorrowedBookResponseDTO;
import org.systemia.book.domain.services.BookService;
import org.systemia.book.domain.services.FileStorageService;
import org.systemia.book.exceptions.OperationNotPermittedException;
import org.systemia.book.infrastructure.mappers.BookMapper;
import org.systemia.book.infrastructure.persistence.entities.Book;
import org.systemia.book.infrastructure.persistence.entities.BookTransactionHistory;
import org.systemia.book.infrastructure.persistence.entities.User;
import org.systemia.book.infrastructure.persistence.repository.BookRepository;
import org.systemia.book.infrastructure.persistence.repository.BookTransactionHistoryRepository;
import org.systemia.book.infrastructure.persistence.repository.specification.BookSpecification;
import org.systemia.book.utils.CycleAvoidingMappingContext;
import org.systemia.book.utils.FileUtils;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static org.systemia.book.infrastructure.persistence.repository.specification.BookSpecification.withOwnerId;

@Service
@RequiredArgsConstructor
public class BookServiceAdapter implements BookService {

	private final BookMapper bookMapper;
	private final BookRepository bookRepository;
	private final BookTransactionHistoryRepository historyRepository;
	private final FileStorageService fileStorageService;

	@Override
	public Long createBook(BookDTO dto, Authentication authentication) {
		User user = (User) authentication.getPrincipal();
		Book book = bookMapper.toEntity(dto, new CycleAvoidingMappingContext());
		book.setOwner(user);

		return bookRepository.save(book).getId();
	}

	@Override
	public BookResponseDTO findByID(Long bookId) {
		return bookRepository.findById(bookId)
			.map(bookMapper::toBookResponse)
			.orElseThrow(() -> new IllegalStateException("Book not found with ID " + bookId));
	}

	@Override
	public Page<BookResponseDTO> findAllBooks(Pageable pageable, Authentication connectedUser) {
		User user = (User) connectedUser.getPrincipal();
		// Page<Book> page = bookRepository.findAllDisplayableBooks(pageable, user.getId());
		var page = bookRepository.findAll(BookSpecification.withOwnerName(user.getId(), ""), pageable);
		return page.map(bookMapper::toBookResponse);
	}

	@Override
	public Page<BookResponseDTO> findAllBooksByOwner(Pageable pageable, Authentication connectedUser) {
		User user = (User) connectedUser.getPrincipal();
		Page<Book> page = bookRepository.findAll(withOwnerId(user.getId()), pageable);
		return page.map(bookMapper::toBookResponse);
	}

	@Override
	public Page<BorrowedBookResponseDTO> findAllBorrowedBooks(Pageable pageable, Authentication connectedUser) {
		User user = (User) connectedUser.getPrincipal();
		Page<BookTransactionHistory> allBorrowedPage = historyRepository.findAllBorrowedBooks(pageable, user.getId());
		return allBorrowedPage.map(bookMapper::toBorrowedBookResponse);
	}

	@Override
	public Page<BorrowedBookResponseDTO> findAllReturnedBooks(Pageable pageable, Authentication connectedUser) {
		User user = (User) connectedUser.getPrincipal();
		Page<BookTransactionHistory> allBorrowedPage = historyRepository.findAllReturnedBooks(pageable, user.getId());
		return allBorrowedPage.map(bookMapper::toBorrowedBookResponse);
	}

	@Override
	public Long updateShareableStatus(Long bookId, Authentication connectedUser) {
		Book book = bookRepository.findById(bookId)
			.orElseThrow(() -> new EntityNotFoundException("No book found with the ID::" + bookId));

		User user = (User) connectedUser.getPrincipal();

		if (!Objects.equals(book.getOwner().getId(), user.getId())) {
			throw new OperationNotPermittedException("You cannot update books shareable status");
		}

		book.setShareable(!book.isShareable());
		bookRepository.save(book);

		return book.getId();
	}

	@Override
	public Long updateArchivedStatus(Long bookId, Authentication connectedUser) {
		Book book = bookRepository.findById(bookId)
			.orElseThrow(() -> new EntityNotFoundException("No book found with the ID::" + bookId));

		User user = (User) connectedUser.getPrincipal();

		if (!Objects.equals(book.getOwner().getId(), user.getId())) {
			throw new OperationNotPermittedException("You cannot update books archived status");
		}

		book.setArchived(!book.isArchived());
		bookRepository.save(book);

		return book.getId();
	}

	@Override
	public Long borrowBook(Long bookId, Authentication connectedUser) {
		Book book = bookRepository.findById(bookId)
			.orElseThrow(() -> new EntityNotFoundException("No book found with the ID::" + bookId));

		if (book.isArchived() || !book.isShareable()) {
			throw new OperationNotPermittedException("The requested book cannot be borrowed since it is archived and not shareable");
		}

		User user = (User) connectedUser.getPrincipal();
		if (!Objects.equals(book.getOwner().getId(), user.getId())) {
			throw new OperationNotPermittedException("You cannot update books archived status");
		}

		final boolean isAlreadyBorrowed = historyRepository.isAlreadyBorrowedByUser(bookId, user.getId());

		if (isAlreadyBorrowed) {
			throw new OperationNotPermittedException("The requested book is already borrowed");
		}

		var bookTransactionHistory = BookTransactionHistory.builder()
			.user(user)
			.book(book)
			.returned(false)
			.returnApproved(false)
			.build();

		return historyRepository.save(bookTransactionHistory).getId();
	}

	@Override
	public Long returnBorrowedBook(Long bookId, Authentication connectedUser) {
		Book book = bookRepository.findById(bookId)
			.orElseThrow(() -> new EntityNotFoundException("No book found with the ID::" + bookId));

		if (book.isArchived() || !book.isShareable()) {
			throw new OperationNotPermittedException("The requested book cannot be borrowed since it is archived and not shareable");
		}

		User user = (User) connectedUser.getPrincipal();
		if (!Objects.equals(book.getOwner().getId(), user.getId())) {
			throw new OperationNotPermittedException("You cannot update books archived status");
		}

		var bookTransactionHistory = historyRepository.findByBookIdAndUserId(bookId, user.getId())
			.orElseThrow(() -> new OperationNotPermittedException("You cannot borrow this book"));

		bookTransactionHistory.setReturned(true);

		return historyRepository.save(bookTransactionHistory).getId();
	}

	@Override
	@Transactional(readOnly = true)
	public List<BookResponseDTO> streamAll() {
		try (var stream = bookRepository.streamAllBy()) {
			return stream
				.map(bookMapper::toBookResponse)
				.toList();
		}
	}

	@Override
	public Long approveReturnBorrowedBook(Long bookId, Authentication connectedUser) {
		Book book = bookRepository.findById(bookId)
			.orElseThrow(() -> new EntityNotFoundException("No book found with the ID::" + bookId));

		if (book.isArchived() || !book.isShareable()) {
			throw new OperationNotPermittedException("The requested book cannot be borrowed since it is archived and not shareable");
		}

		User user = (User) connectedUser.getPrincipal();
		if (!Objects.equals(book.getOwner().getId(), user.getId())) {
			throw new OperationNotPermittedException("You cannot update books archived status");
		}

		var bookTransactionHistory = historyRepository.findByBookIdAndOwnerId(bookId, user.getId())
			.orElseThrow(() -> new OperationNotPermittedException("The book is not returned yet. You cannot approve its return"));

		bookTransactionHistory.setReturned(true);

		return historyRepository.save(bookTransactionHistory).getId();
	}

	@Override
	public void uploadBookCoverPicture(MultipartFile multipartFile, Authentication connectedUser, Long bookId, double resize) throws IOException {
		Book book = bookRepository.findById(bookId)
			.orElseThrow(() -> new EntityNotFoundException("No book found with the ID::" + bookId));

		User user = (User) connectedUser.getPrincipal();
		String bookCoverUrl = fileStorageService.saveFile(multipartFile, book, user.getId());
		byte[] readBytes = FileUtils.resizeImage(multipartFile, resize);

		book.setCover(readBytes);
		book.setBookCover(bookCoverUrl);

		bookRepository.save(book);
	}
}
