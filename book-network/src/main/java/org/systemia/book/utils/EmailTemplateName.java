package org.systemia.book.utils;

import lombok.Getter;

@Getter
public enum EmailTemplateName {
	ACTIVATE_ACCOUNT("activate_account");

	private final String nameTemplate;

	EmailTemplateName(String name) {
		this.nameTemplate = name;
	}

}
