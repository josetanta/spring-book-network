package org.systemia.book.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.util.StringUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;
import java.util.Objects;

@Slf4j
public final class FileUtils {

	private FileUtils() {
	}

	/**
	 * Read location of a file
	 *
	 * @param fileUrl path of file
	 * @return array of bytes
	 */
	public static byte[] readFileFromLocation(String fileUrl) {
		if (StringUtils.isEmpty(fileUrl)) {
			return new byte[0];
		}
		try {
			Path filePath = new File(fileUrl).toPath();
			return Files.readAllBytes(filePath);
		} catch (IOException ex) {
			log.warn("No file found in the path {}", fileUrl);
		}
		return new byte[0];
	}

	public static byte[] readBytesFromString(String bytesInString) {
		if (Objects.isNull(bytesInString)) {
			return new byte[0];
		}
		return Base64.getDecoder().decode(bytesInString);
	}

	public static byte[] resizeImage(MultipartFile file, int targetWidth, int targetHeight) throws IOException {

		if (Objects.isNull(file)) {
			return new byte[0];
		}

		BufferedImage originalImage = ImageIO.read(file.getInputStream());
		Image resultImage = originalImage.getScaledInstance(targetWidth, targetHeight, Image.SCALE_SMOOTH);
		BufferedImage outputImage = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2D = outputImage.createGraphics();
		graphics2D.drawImage(resultImage, 0, 0, null);
		graphics2D.dispose();

		var outputStream = new ByteArrayOutputStream();
		ImageIO.write(outputImage, "png", outputStream);
		outputStream.close();
		return outputStream.toByteArray();
	}

	public static byte[] resizeImage(MultipartFile file, double percentage) throws IOException {

		if (Objects.isNull(file)) {
			return new byte[0];
		}

		BufferedImage originalImage = ImageIO.read(file.getInputStream());

		int targetWidth = (int) (originalImage.getWidth() * (percentage / 100));
		int targetHeight = (int) (originalImage.getHeight() * (percentage / 100));

		Image resultImage = originalImage.getScaledInstance(targetWidth, targetHeight, Image.SCALE_SMOOTH);
		BufferedImage outputImage = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_ARGB);
		Graphics2D graphics2D = outputImage.createGraphics();
		graphics2D.drawImage(resultImage, 0, 0, null);
		graphics2D.dispose();

		var outputStream = new ByteArrayOutputStream();
		ImageIO.write(outputImage, "png", outputStream);
		outputStream.close();
		return outputStream.toByteArray();
	}
}
