package org.systemia.book.application.api.dtos.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BorrowedBookResponseDTO {
	private Long id;
	private String title;
	private String authorName;
	private String isbn;
	private double rate;
	private boolean returned;
	private boolean returnApproved;
}
