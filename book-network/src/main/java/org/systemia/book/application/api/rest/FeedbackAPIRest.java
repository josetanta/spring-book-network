package org.systemia.book.application.api.rest;

import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.systemia.book.application.api.dtos.FeedbackDTO;
import org.systemia.book.application.api.dtos.response.FeedbackResponseDTO;
import org.systemia.book.domain.services.FeedbackService;

@RestController
@RequestMapping("/feedbacks")
@RequiredArgsConstructor
@Tag(name = "Feedback")
public class FeedbackAPIRest {

	private final FeedbackService feedbackService;

	@PostMapping
	public ResponseEntity<Long> postSaveFeedback(
		@Valid @RequestBody FeedbackDTO dto,
		Authentication connectedUser
	) {
		return ResponseEntity.ok(feedbackService.save(dto, connectedUser));
	}

	@GetMapping("/book/{book-id}")
	public ResponseEntity<Page<FeedbackResponseDTO>> getFindAllFeedbacksByBook(
		Pageable pageable,
		Authentication connectedUser,
		@PathVariable("book-id") Long bookId
	) {

		return ResponseEntity.ok(feedbackService.findAllFeedbacksByBook(bookId, pageable, connectedUser));
	}

}
