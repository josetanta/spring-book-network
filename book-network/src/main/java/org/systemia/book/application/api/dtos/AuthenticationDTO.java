package org.systemia.book.application.api.dtos;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public record AuthenticationDTO(
	@NotNull
	@NotEmpty
	String email,

	@NotNull
	@NotEmpty
	@Size(min = 8, max = 25)
	String password
) {
}
