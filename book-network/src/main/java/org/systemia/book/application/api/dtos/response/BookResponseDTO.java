package org.systemia.book.application.api.dtos.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookResponseDTO {
	private Long id;
	private String title;
	private String authorName;
	private String isbn;
	private String synopsis;
	private String owner;
	private byte[] cover;
	private byte[] imageBytes;
	private double rate;
	private boolean archived;
	private boolean shareable;
}
