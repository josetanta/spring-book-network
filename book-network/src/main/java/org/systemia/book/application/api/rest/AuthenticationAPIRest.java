package org.systemia.book.application.api.rest;

import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.mail.MessagingException;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.systemia.book.application.api.dtos.AuthenticationDTO;
import org.systemia.book.application.api.dtos.UserRegisterDTO;
import org.systemia.book.application.api.dtos.response.TokenResponseDTO;
import org.systemia.book.domain.services.AuthenticationService;
import org.systemia.book.utils.GenericResponse;

@CrossOrigin
@RestController
@RequestMapping("auth")
@RequiredArgsConstructor
@Tag(name = "Authentication")
@Slf4j
public class AuthenticationAPIRest {
	private final AuthenticationService service;

	@PostMapping("/register")
	public ResponseEntity<GenericResponse<Long>> postRegister(
		@RequestBody @Valid UserRegisterDTO dto
	) throws MessagingException {
		Long userId = service.createUser(dto);
		GenericResponse<Long> response = GenericResponse.<Long>builder()
			.codeStatus(HttpStatus.ACCEPTED.value())
			.data(userId)
			.message("User created")
			.build();
		return ResponseEntity.accepted().body(response);
	}

	@PostMapping("/authenticate")
	public ResponseEntity<GenericResponse<TokenResponseDTO>> postAuthenticate(
		@RequestBody @Valid AuthenticationDTO dto
	) {
		TokenResponseDTO authenticate = service.authenticate(dto);
		GenericResponse<TokenResponseDTO> response = GenericResponse.<TokenResponseDTO>builder()
			.codeStatus(HttpStatus.OK.value())
			.data(authenticate)
			.message("User authenticated")
			.build();
		log.info("User authenticated - {}", dto);
		return ResponseEntity.ok(response);
	}

	@GetMapping("/activate-account")
	public ResponseEntity<Object> getConfirmAccount(
		@RequestParam String token
	) {
		try {
			service.activateAccount(token);
			log.info("Activation successfully");
			return ResponseEntity.noContent().build();
		} catch (MessagingException | IllegalStateException ex) {
			log.error("Has occurred a problem {}", ex.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}
}
