package org.systemia.book.application.api.dtos;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.io.Serializable;

@Valid
public record UserRegisterDTO(
	@NotNull
	@NotEmpty
	String firstname,

	@NotNull
	@NotEmpty
	String lastname,

	@NotNull
	@NotEmpty
	String email,

	@NotNull
	@NotEmpty
	@Size(min = 8, max = 25)
	String password
) implements Serializable {
}
