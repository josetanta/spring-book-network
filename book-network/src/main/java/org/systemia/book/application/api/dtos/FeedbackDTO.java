package org.systemia.book.application.api.dtos;

import jakarta.validation.constraints.*;

public record FeedbackDTO(
	@Positive(message = "200")
	@Min(value = 0, message = "201")
	@Max(value = 5, message = "202")
	Double note,
	@NotNull
	@NotEmpty
	@NotBlank
	String comment,

	@NotNull
	@Min(value = 1)
	Long bookId
) {
}
