package org.systemia.book.application.api.rest;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.systemia.book.application.api.dtos.BookDTO;
import org.systemia.book.application.api.dtos.response.BookResponseDTO;
import org.systemia.book.application.api.dtos.response.BorrowedBookResponseDTO;
import org.systemia.book.domain.services.BookService;
import org.systemia.book.utils.GenericResponse;

import java.io.IOException;
import java.net.URI;

@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
@Tag(name = "Book")
@Slf4j
public class BookAPIRest {

	private final BookService bookService;

	@PostMapping
	public ResponseEntity<GenericResponse<Long>> postSaveBook(
		@RequestBody @Valid BookDTO dto,
		Authentication authentication
	) {
		Long bookId = bookService.createBook(dto, authentication);
		GenericResponse<Long> response = GenericResponse.<Long>builder()
			.data(bookId)
			.message("Created book")
			.codeStatus(HttpStatus.CREATED.value())
			.build();
		return ResponseEntity
			.created(URI.create("/books"))
			.body(response);
	}

	@GetMapping("/{book-id}")
	public ResponseEntity<GenericResponse<BookResponseDTO>> getBookByID(
		@PathVariable("book-id") Long bookId
	) {
		BookResponseDTO book = bookService.findByID(bookId);
		GenericResponse<BookResponseDTO> response = GenericResponse.<BookResponseDTO>builder()
			.data(book)
			.message("Get a Book")
			.codeStatus(HttpStatus.OK.value())
			.build();
		log.info("Got it a Book with id {}", book.getId());
		return ResponseEntity.ok(response);
	}

	@GetMapping
	public ResponseEntity<GenericResponse<Page<BookResponseDTO>>> getFindAllBooks(
		Pageable pageable,
		Authentication connectedUser
	) {
		var resultpage = bookService.findAllBooks(pageable, connectedUser);
		var response = GenericResponse.<Page<BookResponseDTO>>builder()
			.data(resultpage)
			.message("Page books")
			.codeStatus(HttpStatus.OK.value())
			.build();
		log.info("Filtering and pagination books");
		return ResponseEntity.ok(response);
	}

	@GetMapping("/owner")
	public ResponseEntity<GenericResponse<Page<BookResponseDTO>>> getFindAllBooksByOwner(
		Pageable pageable,
		Authentication connectedUser
	) {
		var result = bookService.findAllBooksByOwner(pageable, connectedUser);
		var response = GenericResponse.<Page<BookResponseDTO>>builder()
			.data(result)
			.message("Page books")
			.codeStatus(HttpStatus.OK.value())
			.build();
		return ResponseEntity.ok(response);
	}

	@GetMapping("/borrowed")
	public ResponseEntity<GenericResponse<Page<BorrowedBookResponseDTO>>> getFindAllBorrowedBooks(
		Pageable pageable,
		Authentication connectedUser
	) {
		var resultPage = bookService.findAllBorrowedBooks(pageable, connectedUser);
		var response = GenericResponse.<Page<BorrowedBookResponseDTO>>builder()
			.data(resultPage)
			.message("Page books")
			.codeStatus(HttpStatus.OK.value())
			.build();
		return ResponseEntity.ok(response);
	}

	@GetMapping("/returned")
	public ResponseEntity<GenericResponse<Page<BorrowedBookResponseDTO>>> getFindAllReturnedBooks(
		Pageable pageable,
		Authentication connectedUser
	) {
		var resultPage = bookService.findAllReturnedBooks(pageable, connectedUser);
		var response = GenericResponse.<Page<BorrowedBookResponseDTO>>builder()
			.data(resultPage)
			.message("Page books")
			.codeStatus(HttpStatus.OK.value())
			.build();
		return ResponseEntity.ok(response);
	}

	@PatchMapping("/shareable/{book-id}")
	public ResponseEntity<GenericResponse<Long>> patchUpdateShareableStatus(
		@PathVariable("book-id") Long bookId,
		Authentication connectedUser
	) {
		Long result = bookService.updateShareableStatus(bookId, connectedUser);
		var response = GenericResponse.<Long>builder()
			.data(result)
			.message("Get book id")
			.codeStatus(HttpStatus.OK.value())
			.build();
		return ResponseEntity.ok(response);
	}

	@PatchMapping("/archived/{book-id}")
	public ResponseEntity<GenericResponse<Long>> patchUpdateArchivedStatus(
		@PathVariable("book-id") Long bookId,
		Authentication connectedUser
	) {
		Long result = bookService.updateArchivedStatus(bookId, connectedUser);
		var response = GenericResponse.<Long>builder()
			.data(result)
			.message("Get book id")
			.codeStatus(HttpStatus.OK.value())
			.build();
		return ResponseEntity.ok(response);
	}

	@PostMapping("/borrow/{book-id}")
	public ResponseEntity<GenericResponse<Long>> postBorrowBook(
		@PathVariable("book-id") Long bookId,
		Authentication connectedUser
	) {
		Long result = bookService.borrowBook(bookId, connectedUser);
		var response = GenericResponse.<Long>builder()
			.data(result)
			.message("Get book id borrow")
			.codeStatus(HttpStatus.OK.value())
			.build();
		return ResponseEntity.ok(response);
	}

	@PatchMapping("/borrow/return/{book-id}")
	public ResponseEntity<GenericResponse<Long>> patchReturnBorrowBook(
		@PathVariable("book-id") Long bookId,
		Authentication connectedUser
	) {
		Long result = bookService.returnBorrowedBook(bookId, connectedUser);
		var response = GenericResponse.<Long>builder()
			.data(result)
			.message("Get book id borrow")
			.codeStatus(HttpStatus.OK.value())
			.build();
		return ResponseEntity.ok(response);
	}

	@PatchMapping("/borrow/return/approve/{book-id}")
	public ResponseEntity<GenericResponse<Long>> patchReturnApproveBorrowBook(
		@PathVariable("book-id") Long bookId,
		Authentication connectedUser
	) {
		Long result = bookService.approveReturnBorrowedBook(bookId, connectedUser);
		var response = GenericResponse.<Long>builder()
			.data(result)
			.message("Get book id approved borrow")
			.codeStatus(HttpStatus.OK.value())
			.build();
		return ResponseEntity.ok(response);
	}

	@PostMapping(path = "/cover/{book-id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<GenericResponse<?>> postUploadBookCoverPicture(
		@PathVariable("book-id") Long bookId,
		@Parameter @RequestPart("file") MultipartFile multipartFile,
		@RequestParam(name = "resize-percentage", defaultValue = "100") double resize,
		Authentication connectedIUser
	) throws IOException {
		var response = GenericResponse.builder()
			.data(null)
			.message("Get book id approved borrow")
			.codeStatus(HttpStatus.OK.value())
			.build();
		bookService.uploadBookCoverPicture(multipartFile, connectedIUser, bookId, resize);
		return ResponseEntity.accepted().body(response);
	}

	@GetMapping("/stream")
	public ResponseEntity<GenericResponse<?>> getStreamAllBook() {
		return ResponseEntity.ok(GenericResponse.builder()
			.data(bookService.streamAll())
			.build());
	}
}
