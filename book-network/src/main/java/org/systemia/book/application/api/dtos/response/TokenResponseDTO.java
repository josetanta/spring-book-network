package org.systemia.book.application.api.dtos.response;

public record TokenResponseDTO(
	String token
) {
}
