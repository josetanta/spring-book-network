package org.systemia.book.application.api.dtos.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FeedbackResponseDTO {

	private Double note;
	private String comment;
	private boolean ownFeedback;

}
